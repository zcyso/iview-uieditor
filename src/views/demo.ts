
export const DemoJson = {
  "children": [
    {
      "type": "Col",
      "props": {
        "span": 18
      },
      "children": [
        {
          "type": "Card",
          "props": {
            "class": "mb-sm",
            "title": "Card 卡片"
          },
          "children": [
            {
              "type": "iview-uieditor-table",
              "props": {
                ":data": "[\n  {\n    name:'name',\n    age:'age',\n    address:'address'\n  }\n]",
                ":columns": "[\n  {\n    title: 'Name',\n    key: 'name',\n    slot: 'name'\n  },\n  {\n    title: 'Age',\n    key: 'age'\n  },\n  {\n    title: 'Address',\n    key: 'address'\n  }\n]",
                ":show-header": "true"
              },
              "children": [
                {
                  "type": "template",
                  "props": {
                    "slot": "name",
                    "slot-scope": "{ row, index }"
                  },
                  "children": [
                    {
                      "children": [
                        {
                          "type": "uieditor-text",
                          "props": {
                            ":text": "row.name"
                          }
                        }
                      ],
                      "type": "uieditor-a"
                    }
                  ]
                }
              ]
            },
            {
              "type": "Page",
              "props": {
                "class": "mt-md",
                ":current": 1,
                ":total": 50,
                ":page-size": 10,
                ":page-size-opts": "[10,20,30,40]",
                ":show-total": "true",
                ":show-sizer": "true",
                ":disabled": "false"
              }
            }
          ]
        },
        {
          "type": "Card",
          "props": {
            "class": "mb-sm",
            "title": "Card 卡片"
          },
          "children": [
            {
              "children": [
                {
                  "type": "Row",
                  "props": {
                    ":gutter": 16,
                    "ue-cant-select": true,
                    "ue-cant-remove": true,
                    "ue-cant-move": true,
                    "ue-cant-movein": true,
                    "ue-cant-moveout": true
                  },
                  "children": [
                    {
                      "type": "Col",
                      "props": {
                        "span": 24,
                        "ue-cant-move-child": true,
                        "ue-cant-movein": true,
                        "ue-cant-remove-child": true,
                        "ue-cant-copy-child": true
                      },
                      "children": [
                        {
                          "type": "FormItem",
                          "props": {
                            "label": "Input："
                          },
                          "children": [
                            {
                              "type": "Input",
                              "props": {
                                "type": "text"
                              }
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "type": "Col",
                      "props": {
                        "span": 12,
                        "ue-cant-move-child": true,
                        "ue-cant-movein": true,
                        "ue-cant-remove-child": true,
                        "ue-cant-copy-child": true
                      },
                      "children": [
                        {
                          "type": "FormItem",
                          "props": {
                            "label": "InputNum："
                          },
                          "children": [
                            {
                              "type": "InputNumber",
                              "props": {
                                "size": "default"
                              }
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "type": "Col",
                      "props": {
                        "span": 12,
                        "ue-cant-move-child": true,
                        "ue-cant-movein": true,
                        "ue-cant-remove-child": true,
                        "ue-cant-copy-child": true
                      },
                      "children": [
                        {
                          "type": "FormItem",
                          "props": {
                            "label": "Select："
                          },
                          "children": [
                            {
                              "type": "Select",
                              "children": [
                                {
                                  "type": "Option",
                                  "props": {
                                    "value": "1",
                                    "label": "选项1"
                                  }
                                }
                              ]
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "type": "Col",
                      "props": {
                        "span": 24,
                        "ue-cant-move-child": true,
                        "ue-cant-movein": true,
                        "ue-cant-remove-child": true,
                        "ue-cant-copy-child": true
                      },
                      "children": [
                        {
                          "type": "FormItem",
                          "props": {
                            "label": "Textarea："
                          },
                          "children": [
                            {
                              "type": "Input",
                              "props": {
                                "type": "textarea",
                                ":rows": "3"
                              }
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "type": "Col",
                      "props": {
                        "span": 24,
                        "ue-cant-move-child": true,
                        "ue-cant-movein": true,
                        "ue-cant-remove-child": true,
                        "ue-cant-copy-child": true
                      },
                      "children": [
                        {
                          "type": "FormItem",
                          "children": [
                            {
                              "type": "Button",
                              "children": [
                                {
                                  "type": "Icon",
                                  "props": {
                                    "class": "mr-sm",
                                    "type": "ios-heart-outline",
                                    "ue-cant-move": true
                                  }
                                },
                                {
                                  "type": "uieditor-text",
                                  "props": {
                                    "text": "提交",
                                    "ue-cant-move": true
                                  }
                                }
                              ],
                              "props": {
                                "class": "mr-sm",
                                "type": "primary"
                              }
                            },
                            {
                              "type": "Button",
                              "children": [
                                {
                                  "type": "Icon",
                                  "props": {
                                    "class": "mr-sm",
                                    "type": "ios-heart-outline",
                                    "ue-cant-move": true
                                  }
                                },
                                {
                                  "type": "uieditor-text",
                                  "props": {
                                    "text": "取消",
                                    "ue-cant-move": true
                                  }
                                }
                              ],
                              "props": {
                                "class": "mr-sm"
                              }
                            }
                          ],
                          "editor-attrs": {
                            "label": ""
                          }
                        }
                      ]
                    }
                  ]
                }
              ],
              "type": "Form",
              "props": {
                "class": "pt-lg",
                ":label-width": "120"
              }
            }
          ]
        }
      ]
    },
    {
      "type": "Col",
      "props": {
        "span": 6
      },
      "children": [
        {
          "type": "Card",
          "props": {
            "class": "mb-sm",
            "title": "Card 卡片"
          },
          "children": [
            {
              "children": [
                {
                  "type": "Row",
                  "props": {
                    ":gutter": 16,
                    "ue-cant-select": true,
                    "ue-cant-remove": true,
                    "ue-cant-move": true,
                    "ue-cant-movein": true,
                    "ue-cant-moveout": true
                  },
                  "children": [
                    {
                      "type": "Col",
                      "props": {
                        "span": 24,
                        "ue-cant-move-child": true,
                        "ue-cant-movein": true,
                        "ue-cant-remove-child": true,
                        "ue-cant-copy-child": true
                      },
                      "children": [
                        {
                          "type": "FormItem",
                          "props": {
                            "label": "名称："
                          },
                          "children": [
                            {
                              "type": "uieditor-text",
                              "props": {
                                "text": "name"
                              }
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "type": "Col",
                      "props": {
                        "span": 24,
                        "ue-cant-move-child": true,
                        "ue-cant-movein": true,
                        "ue-cant-remove-child": true,
                        "ue-cant-copy-child": true
                      },
                      "children": [
                        {
                          "type": "FormItem",
                          "props": {
                            "label": "类型："
                          },
                          "children": [
                            {
                              "type": "uieditor-text",
                              "props": {
                                "text": "type"
                              }
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ],
              "type": "Form",
              "props": {
                "class": "pt-lg",
                ":label-width": "60"
              }
            }
          ]
        },
        {
          "type": "Card",
          "props": {
            "class": "mb-sm",
            "title": "Card 卡片"
          },
          "children": [
            {
              "children": [
                {
                  "type": "Row",
                  "props": {
                    ":gutter": 16,
                    "ue-cant-select": true,
                    "ue-cant-remove": true,
                    "ue-cant-move": true,
                    "ue-cant-movein": true,
                    "ue-cant-moveout": true
                  },
                  "children": [
                    {
                      "type": "Col",
                      "props": {
                        "span": 24,
                        "ue-cant-move-child": true,
                        "ue-cant-movein": true,
                        "ue-cant-remove-child": true,
                        "ue-cant-copy-child": true
                      },
                      "children": [
                        {
                          "type": "FormItem",
                          "props": {
                            "label": "名称："
                          },
                          "children": [
                            {
                              "type": "uieditor-text",
                              "props": {
                                "text": "name"
                              }
                            }
                          ]
                        }
                      ]
                    },
                    {
                      "type": "Col",
                      "props": {
                        "span": 24,
                        "ue-cant-move-child": true,
                        "ue-cant-movein": true,
                        "ue-cant-remove-child": true,
                        "ue-cant-copy-child": true
                      },
                      "children": [
                        {
                          "type": "FormItem",
                          "props": {
                            "label": "类型："
                          },
                          "children": [
                            {
                              "type": "uieditor-text",
                              "props": {
                                "text": "type"
                              }
                            }
                          ]
                        }
                      ]
                    }
                  ]
                }
              ],
              "type": "Form",
              "props": {
                "class": "pt-lg",
                ":label-width": "60"
              }
            }
          ]
        }
      ]
    }
  ],
  "type": "Row",
  "props": {
    ":gutter": 16
  }
};